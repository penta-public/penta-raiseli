import test from 'ava';

test('async test', (t) => {
	return Promise.resolve('wecodetheweb').then((text) => {
		t.is(text, 'wecodetheweb');
	});
});

test('one plus one is two', (t) => {
	t.is(1 + 1, 2);
});

test('two plus one is three', (t) => {
	t.is(1 + 2, 3);
});
