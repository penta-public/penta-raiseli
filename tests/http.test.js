'use strict';

const http = require('http');
const test = require('ava');
//const got = require('got');
const listen = require('test-listen');
const penta = require('../penta-raiseli');

/**
 * Penta RequestListener Module.
 * @module RequestListener
 */
const testGroup = 'HTTP';
const path = '/helvetica';

const testEndpoint = (request, response) => {
	const { headers, method, url } = request;
	let body = [];
	if (method === 'POST') {
		response.statusCode = 200; // Tell the client that the request is OK
		request.on('data', (chunk) => {
			body.push(chunk);
		});
		request.on('end', () => {
			body = Buffer.concat(body).toString();
			response.setHeader('Content-Type', 'application/json');
			const responseBody = { headers, method, url, body };
			response.write(JSON.stringify(responseBody));
			response.end();
		});
	} else {
		response.statusCode = 500;
	}

	// magic happens here!
};

test.before(async (t) => {
	t.context.server = http.createServer(testEndpoint);
	t.context.baseURL = await listen(t.context.server);
	t.log(`\n========================  ${testGroup}  =========================`);
	t.log(`httpRequest server created and listening ${t.context.baseURL}`);
});

test.after.always((t) => {
	t.context.server.close();
});

/**
 * AVA test for sending http requests to Penta microservices platform
 * See {@link https://medium.com/allenhwkim/ava-the-test-tool-that-works-5d98ee03933e}
 */
test('httpRequest /', async (t) => {
	const d = { email: 'smelnikoff@mac.com', json: true, x: 'string', path: '/' };
	const url = t.context.baseURL;
	const response = await penta.httpRequest(url, d);
	t.log('http Response:\n', JSON.stringify(response, null, 2), '\n------------------');

	t.deepEqual(response.data.body, d); //check entire object
});

test('httpRequest /testpath', async (t) => {
	const d = {
		email: 'smelnikoff@mac.com',
		json: true,
		x: 'string',
		path: path
	};
	const url = `${t.context.baseURL}/testpath`;
	const response = await penta.httpRequest(url, d);
	t.log('http /testpath Response:\n', JSON.stringify(response, null, 2), '\n------------------');

	t.deepEqual(response.data.body, d); //check entire object
});
