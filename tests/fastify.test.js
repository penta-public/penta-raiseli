'use strict';

// Require the framework and instantiate it
// npm run test
const fastify = require('fastify')({ logger: false });
const test = require('ava');
const got = require('got');
//const listen = require('test-listen');
const penta = require('../penta-raiseli');

/**
 * Penta Fastify Listener Module.
 * @module RequestListener
 */
const testGroup = 'FASTIFY';
const port = 9001;
const paths = { http: '/http', helvetica: '/helvetica' };
const httpRoute = {
	path: '/http',
	data: null,
	filepath: './routes/http',
	schema: null
};
const defaultRoute = {
	path: '/',
	data: { hello: 'world' },
	filepath: './routes/default',
	schema: {
		response: {
			200: {
				type: 'object',
				properties: {
					hello: { type: 'string' }
				}
			}
		}
	}
};
const helveticaRoute = {
	path: '/helvetica', //exports the fastify-cote options, filepath relative to plugins/
	data: null,
	cote: '../routes/helvetica',
	filepath: './routes/helvetica', //pseudo-filepath, actually points to fastify-cote module
	pluginPath: './plugins/fastify-cote', //pseudo-filepath, actually points to fastify-cote module
	schema: null
};
const payload = {
	email: 'smelnikoff@mac.com',
	json: true,
	x: 'string',
	path: paths.http //defined at top level
};
test.before('fastify test before', async (t) => {
	// Run the server!
	try {
		t.log(`\n========================  ${testGroup}  =========================`);
		fastify.register(require(httpRoute.filepath).route, httpRoute);
		t.log(`registered ${httpRoute.filepath}`);
		fastify.register(require(defaultRoute.filepath).route, defaultRoute);
		t.log(`registered ${defaultRoute.filepath}`);
		fastify.register(require(helveticaRoute.pluginPath), helveticaRoute);
		t.log(`registered ${helveticaRoute.pluginPath}`);
		fastify.register(require(helveticaRoute.filepath).route, helveticaRoute);
		t.log(`registered ${helveticaRoute.filepath}`);
		await fastify.listen(port);
		t.log(`** Fastify server listening on ${fastify.server.address().port}`);
		t.log(`fastify routes\n ${fastify.printRoutes()}`);
	} catch (err) {
		console.log(err);
		process.exit(0);
	}
});

test.after.always('fastify after', async (t) => {
	try {
		await fastify.close();
		t.log('fastify successfully closed!');
	} catch (err) {
		t.log('fastify close error happened', err);
	}
});

test('/ fastify statusCode', async (t) => {
	await fastify.ready();
	const address = fastify.server.address();
	const url = `http://${address.address}:${address.port}/`;
	const response = await got(url);
	t.is(response.statusCode, 200);
});
test('fastify / headers', async (t) => {
	await fastify.ready();
	const address = fastify.server.address();
	const url = `http://${address.address}:${address.port}/`;
	const response = await got(url);
	t.is(response.headers['content-type'], 'application/json; charset=utf-8');
});
test('fastify / response', async (t) => {
	await fastify.ready();
	const address = fastify.server.address();
	const url = `http://${address.address}:${address.port}/`;
	const response = await got(url);
	const d = JSON.parse(response.body);
	t.deepEqual(d, defaultRoute.data); //check entire object
});
test('/http response', async (t) => {
	await fastify.ready();
	const address = fastify.server.address();
	const url = `http://${address.address}:${address.port}${paths.http}`;
	const response = await penta.httpRequest(url, payload);
	t.log('Fastify helvetica http Response:\n', JSON.stringify(response, null, 2), '\n------------------');
	t.deepEqual(response.data.body, payload); //check entire object
});
test('/helvetica test', async (t) => {
	await fastify.ready();
	const requester = fastify.getRequester('helvetica');
	requester ? t.pass() : t.fail();
});

test('/helvetica send', async (t) => {
	await fastify.ready();
	const requester = fastify.getRequester('helvetica');
	const payload = { data: 'world', meta: { v: 1, w: 'test' }, err: null };
	const response = await requester.send({ type: 'helvetica.echo', payload: payload });
	console.log(response);
	requester && response ? t.pass() : t.fail();
});

test('httpRequest -> fastify -> Cote /helvetica response test', async (t) => {
	await fastify.ready();
	const payload = { data: 'world', meta: { v: 1, w: 'test' }, err: null };
	t.log(`request redux payload: ${JSON.stringify(payload)}`);
	//	const requester = fastify.getRequester('helvetica');
	const address = fastify.server.address();
	const url = `http://${address.address}:${address.port}${paths.helvetica}`;
	const response = await penta.httpRequest(url, payload);
	t.log('Fastify helvetica Response:\n', JSON.stringify(response, null, 2), '\n------------------');
	//	const response = await requester.send({ type: 'helvetica.echo', payload: payload });
	t.pass();
	t.log(response);
	//	requester && response ? t.pass() : t.fail();
});
