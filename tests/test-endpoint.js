"use strict";

/**
 * Penta RequestListener Module.
 * @module RequestListener
 */
const { parse } = require("querystring");

module.exports = (request, response) => {
  const { headers, method, url } = request;
  let body = [];
  if (method === "POST") {
    response.statusCode = 200; // Tell the client that the request is OK
    request.on("data", chunk => {
      body.push(chunk);
    });
    request.on("end", () => {
      body = Buffer.concat(body).toString();
      response.setHeader("Content-Type", "application/json");
      const responseBody = { headers, method, url, body };
      response.write(JSON.stringify(responseBody));
      response.end();
    });
  } else {
    response.statusCode = 500;
  }

  // magic happens here!
};
