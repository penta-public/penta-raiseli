'use strict';

// Require the framework and instantiate it
// npm run test
const fastify = require('fastify')({ logger: false });
const test = require('ava');
const got = require('got');
//const listen = require('test-listen');
const Penta = require('../penta-raiseli');

/**
 * Penta Fastify Listener Module.
 * @module RequestListener
 */
const testGroup = 'RAISELI';
const port = 9000;
const paths = { http: '/http', raiseli: '/raiseli' };
const httpRoute = {
	path: '/http',
	data: null,
	filepath: './routes/http',
	schema: null
};
const defaultRoute = {
	path: '/',
	data: { hello: 'world' },
	filepath: './routes/default',
	schema: {
		response: {
			200: {
				type: 'object',
				properties: {
					hello: { type: 'string' }
				}
			}
		}
	}
};
const raiseliRoute = {
	path: '/raiseli', //exports the fastify-cote options, filepath relative to plugins/
	data: null,
	cote: '../routes/raiseli',
	filepath: './routes/raiseli', //pseudo-filepath, actually points to fastify-cote module in plugins
	pluginPath: './plugins/fastify-cote', //pseudo-filepath, actually points to fastify-cote module in plugins
	schema: null
};
const payload = {
	email: 'smelnikoff@mac.com',
	json: true,
	x: 'string',
	path: paths.http //defined at top level
};

test.before('action test before', async (t) => {
	// Run the server!
	try {
		t.log(`\n========================  ${testGroup}  =========================`);
		fastify.register(require(httpRoute.filepath).route, httpRoute);
		t.log(`registered ${httpRoute.filepath}`);
		fastify.register(require(defaultRoute.filepath).route, defaultRoute);
		t.log(`registered ${defaultRoute.filepath}`);
		fastify.register(require(raiseliRoute.pluginPath), raiseliRoute);
		t.log(`registered ${raiseliRoute.pluginPath}`);
		fastify.register(require(raiseliRoute.filepath).route, raiseliRoute);
		t.log(`registered ${raiseliRoute.filepath}`);
		await fastify.listen(port);
		t.log(`** Raiseli server listening on ${fastify.server.address().port}`);
		t.log(`fastify routes\n ${fastify.printRoutes()}`);
	} catch (err) {
		console.log(err);
		process.exit(0);
	}
});

test.after.always('action test after', async (t) => {
	try {
		await fastify.close();
		t.log('fastify successfully closed!');
	} catch (err) {
		t.log('fastify close error happened', err);
	}
});

test('/ createAction', async (t) => {
	/*
    const createAction = function(
	type = 'ECHO',
	data = {},
	method = 'POST',
	url = 'http://penta.systems:8085',
	timeout = 10000,
	feature = null,
    namespace = 'raiseli',
    err = null
) {
    */
	await fastify.ready();
	const method = 'POST';
	const address = fastify.server.address();
	const url = `http://${address.address}:${address.port}/`;
	const timeout = 10000;
	const feature = null;
	const namespace = 'raiseli';
	const _action = {
		type: 'ECHO',
		payload: payload,
		meta: { method, url, timeout, feature },
		err: null
	};

	const action = Penta.createAction('ECHO', payload, 'POST', url, 10000, null, namespace, null);
	_action.type = `${namespace}.${_action.type}`;
	t.log('Raiseli _action in:\n', JSON.stringify(_action, null, 2), '\n');
	t.log('Raiseli action back:\n', JSON.stringify(action, null, 2), '\n------------------');

	t.deepEqual(_action, action);
});

test('action dispatch -> fastify -> Cote /raiseli response test', async (t) => {
	await fastify.ready();

	const method = 'POST';
	const address = fastify.server.address();
	const url = `http://${address.address}:${address.port}${paths.raiseli}`;
	const timeout = 10000;
	const feature = null;
	const namespace = 'raiseli';
	const _action = {
		type: 'ECHO',
		payload: payload,
		meta: { method, url, timeout, feature },
		err: null
	};

	const action = Penta.createAction('ECHO', payload, 'POST', url, 10000, null, namespace, null);
	_action.type = `${namespace}.${_action.type}`;

	t.log(`request redux action: ${JSON.stringify(action)}`);
	const response = await Penta.dispatch(action);
	t.log('Fastify raiseli Response:\n', JSON.stringify(response, null, 2), '\n------------------');
	//	const response = await requester.send({ type: 'helvetica.echo', payload: payload });
	t.pass();
	t.log(response);
	//	requester && response ? t.pass() : t.fail();
});
