'use strict';

/**
 * Penta Fastify-Cote Microservices Helvetica api  Module.
 * @module fastifyCote
 */

const echo = async function(payload) {
	console.log(`raiseli.js echo fcn returns: ${JSON.stringify(payload)}`);
	return payload;
	//fastify is 'this'
};
const config = {
	requester: {
		decorator: 'getRequester', //fastify.getRequester
		instances: [ { type: 'raiseli', config: { name: 'raiseli requester', key: 'raiseli' } } ]
	},
	responder: {
		instance: { type: 'raiseli', config: { name: 'raiseli responder', key: 'raiseli' } },
		decorator: 'action', //fastify.run
		actions: {
			'raiseli.echo': {
				event: 'raiseli.echo',
				listener: echo
			}
		}
	}
};

/**
 * Plugin to handle the default route '/' using fastify register.
 * Encapsulates fastify instance so all plugins are independent
 * See {@link https://github.com/fastify/fastify/blob/master/docs/Plugins-Guide.md}
 * See {@link https://github.com/fastify/fastify/blob/master/docs/Plugins.md}
 * See {@link https://github.com/fastify/fastify/blob/master/docs/Validation-and-Serialization.md}
 * See this for JSON schema {@link https://json-schema.org/understanding-json-schema/}
 * See {@link https://devhints.io/fastify}
 * @param   {object} fastify - a new fastify instance (context) independent of others
 * @param   {object} options - the options object passed from register method, {path, data, routePath}
 * @param   {string} filepath - where to find the this file, relative to the register
 * @returns  void
 */
async function route(fastify, options, done) {
	const opts = options.schema ? [ options.path, options.schema ] : [ options.path ];
	const requester = fastify.getRequester('raiseli');
	fastify.post(...opts, async (request, response) => {
		const body = {
			headers: request.headers,
			method: request.raw.method,
			url: `http://${request.raw.hostname}${request.raw.url}`,
			body: request.body
		};

		response.header('Content-Type', 'application/json');
		response.statusCode = 200;
		return body;
	});
	done();
}

module.exports = { config, route };
