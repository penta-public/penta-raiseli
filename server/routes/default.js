'use strict';

/**
 * Penta Fastify api default route Module.
 * @module fastifyDefault
 */

/**
 * Plugin to handle the default route '/' using fastify register.
 * Encapsulates fastify instance so all plugins are independent
 * See {@link https://github.com/fastify/fastify/blob/master/docs/Plugins-Guide.md}
 * See {@link https://github.com/fastify/fastify/blob/master/docs/Plugins.md}
 * See {@link https://github.com/fastify/fastify/blob/master/docs/Validation-and-Serialization.md}
 * See this for JSON schema {@link https://json-schema.org/understanding-json-schema/}
 * See {@link https://devhints.io/fastify}
 * @param   {object} fastify - a new fastify instance (context) independent of others
 * @param   {object} options - the options object passed from register method, {path, data, routePath}
 * @param   {string} filepath - where to find the this file, relative to the register
 * @returns  void
 */
async function route(fastify, options, done) {
	const opts = options.schema ? [ options.path, options.schema ] : [ options.path ];
	fastify.get(...opts, async (request, response) => {
		response.header('Content-Type', 'application/json');
		return options.data;
	});
	done();
}

module.exports = { route };
