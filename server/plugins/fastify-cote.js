"use strict";

/**
 * Penta Fastify-Cote Microservices Helvetica api  Module.
 * @module fastifyCotePlugin
 */
const plugin = require("fastify-plugin");
const CoteRequester = require("cote").Requester;
const CoteResponder = require("cote").Responder;

/**
 * Plugin to handle bridge between Fastify and Cote microservices using fastify register.
 * Copy of fastify-cote from GitHub Oskang09
 * Encapsulates fastify instance so all plugins are independent
 * See {@link https://github.com/fastify/fastify/blob/master/docs/Plugins-Guide.md}
 * See {@link https://github.com/fastify/fastify/blob/master/docs/Plugins.md}
 * See {@link https://github.com/fastify/fastify/blob/master/docs/Validation-and-Serialization.md}
 * See {@link https://devhints.io/fastify}
 * See {@link https://github.com/Oskang09/fastify-cote}
 * See {@link http://cote.js.org/}
 * @param   {object} fastify - a new fastify instance (context) independent of others
 * @param   {object} options - the options object passed from register method, {path, data, routePath}
 * @param   {string} filepath - where to find the this file, relative to the register
 * @returns  void
 */

/* const fastifyCotePlugin = function(fastify, opts, done) {
	fastify.decorate('cotex', (d) => console.log(`fastifyCotePlugin called, d:${d}`));
	return done();
}; */

/* const raiseliRoute = {
  path: "/raiseli", //exports the fastify-cote options, filepath relative to plugins/
  data: null,
  modules: {
    cote: "tests/routes/raiseli",
    filepath: "tests/routes/raiseli", //pseudo-filepath, actually points to fastify-cote module in plugins
    pluginPath: "tests/plugins/fastify-cote" //pseudo-filepath, actually points to fastify-cote module in plugins
  },
  schema: null
}; */

const fastifyCotePlugin = async function(fastify, opts, done) {
  const _cote = require(opts.modules.cote); //the module.exports from cote.js = {config}
  opts = _cote.config;
  const noop = () => {};
  let _requesters = {};
  //maps from fastify path handler to cote requesters
  //redux action: {type===name, payload===data, meta, err}
  /*
    requester: {
		decorator: 'getRequester', //fastify.getRequester
		instances: [ { type: 'helvetica', config: { name: 'helvetica requester', key: 'helvetica' } } ]
    }
    */
  const handleRequesters = (instances = [], decorator = "getRequester") => {
    // name === type from redux action
    instances.map(d => {
      //			helvetica: new CoteRequester({ name: 'helvetica requester', key: 'helvetica' })

      const requester = new CoteRequester(d.config);
      _requesters[d.type] = requester;
    });
    fastify.decorate(decorator, type => {
      if (!_requesters[type]) {
        throw new Error(`No Cote Requester of name: ${type}`);
      }
      return _requesters[type];
    });
  };
  /*
    responder: {
        instance: { type: 'helvetica', config: { name: 'helvetica requester', key: 'helvetica' } },
		decorator: 'action', //fastify.run
		actions: {
			helvetica.echo: { //key is the type
				event: 'helvetica.echo', //event sent out
				listener: echo //callback fcn
			}
		}
    }
    */
  const handleResponders = (instance, decorator = "action", actions = {}) => {
    let responder = new CoteResponder(instance.config);
    for (const type of Object.keys(actions)) {
      const action = actions[type];
      responder.on(action.event, payload => action.listener(payload));
      actions[type] = action.listener;
    }
    fastify.decorate(decorator, (type, payload) => {
      if (!actions[type]) {
        throw Error(`Responder ${type} doesn't exists.`);
      }
      return actions[type](payload);
    });
  };
  opts.requester
    ? handleRequesters(opts.requester.instances, opts.requester.decorator)
    : noop();
  opts.responder
    ? handleResponders(
        opts.responder.instance,
        opts.responder.decorator,
        opts.responder.actions
      )
    : noop();
  return done();
};

module.exports = plugin(fastifyCotePlugin);
//module.exports = fastifyCotePlugin;
