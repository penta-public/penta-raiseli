'use strict';

/**
 * Penta Post Module.
 * @module penta
 */

const http = require('http');

const _options = {
	protocol: 'http',
	host: '216.75.58.97',
	port: '8085',
	path: '/testing',
	method: 'POST',
	headers: {
		'Content-Type': 'application/json',
		'Content-Length': 0
	}
}; //eo _options

const _cb = (d) => process.stdout.write(d);
const _log = (msgs) => `${new Date().toLocaleString()} ${msgs.join(' ')}`;

/**
 * Take a url object and return http request options for input to post
 * @param   {object} urlObject - URL object
 * @param   {string} method - POST, GET, HEAD
 * @returns {object} options - object with the http request options
 */
const httpOptions = function(urlObject, method = 'POST') {
	if (urlObject) {
		return {
			method: method,
			hostname: urlObject.hostname,
			port: urlObject.port,
			path: urlObject.pathname,
			headers: {
				'Content-Type': 'application/json'
			}
		};
	} else {
		return _options;
	}
};

/**
 * Create an action using the given arguments
 * Also check out {@link https://redux.js.org/basics/actions}
 * @param   {string} action - input action string: API_REQUEST
 * @param   {object} data - input data object
 * @param   {string} method - POST
 * @param   {string} url - https://penta.systems:8085
 * @param   {number} timeout - 60000 timeout until we give up
 * @param   {string} feature - a little extra for the meta data
 * @param   {string} namespace - which project, 'raiseli' or 'helvetica'?
 * @param   {string} err - error string
 * @returns  {object} Action - object with the parsed action
 */
const createAction = function(
	type = 'ECHO',
	data = {},
	method = 'POST',
	url = 'http://penta.systems:8085',
	timeout = 10000,
	feature = null,
    namespace = 'raiseli',
    err = null
) {
	return {
		type: `${namespace}.${type}`,
		payload: data,
        meta: { method, url, timeout, feature },
        err: err
	};
};

/**
 * Parse a url string and return a URL object for input to post
 * Also check out {@link https://nodejs.org/api/url.html#url_class_url}
 * @example <caption>Usage Example</caption>
 * const _url = penta.url('https://apple.com:8888/get);
 * @param   {string} url - input url string
 * @returns  {object} URL - object with the parsed url string
 */
const url = function(url) {
	try {
		return new URL(url);
	} catch (error) {
		throw new Error(`Invalid url ${url}; ${error}`);
	}
};

/**
 * Send a http action request from Penta to Raiseli.
 * See {@link https://flaviocopes.com/node-http-post/}
 * See {@link https://stackoverflow.com/questions/38533580/nodejs-how-to-promisify-http-request-reject-got-called-two-times}
 * @param   {object} action - the Redux action.
 * @returns  {object} promise - promise, fulfilled with response object
 */
function dispatch(action) {
	return new Promise((resolve, reject) => {
		httpRequest(null, null, null, action).then(resolve, reject);
	});
}

/**
 * Send a http request from Penta to Raiseli.
 * See {@link https://flaviocopes.com/node-http-post/}
 * See {@link https://stackoverflow.com/questions/38533580/nodejs-how-to-promisify-http-request-reject-got-called-two-times}
 * @param   {string} url - a url string or the httpRequest options
 * @param   {object} data - The Cote requestor data object.
 * @param   {string} method - POST, GET, or HEAD (no PUT or DELETE)
 * @returns  {object} promise - promise, fulfilled with response object
 */
function httpRequest(url, data = null, method = 'POST', action = null) {
    if(action) {
        method = action.meta.method;
        url = action.meta.url;
        data = action;
    }
	method = method.toUpperCase();
	if (![ 'GET', 'POST', 'HEAD' ].includes(method)) {
		throw new Error(`Invalid method: ${method}`);
	}

	let urlObject;
	let options;
	try {
		urlObject = new URL(url);
		options = httpOptions(urlObject);
	} catch (error) {
		throw new Error(`Invalid url ${url}; ${error}`);
	}

	if (data) {
		if (method === 'POST') {
			data = JSON.stringify(data);
			options.headers['Content-Length'] = Buffer.byteLength(data);
		} else {
			throw new Error(`Invalid use of the body parameter while using the ${method} method.`);
		}
	}

	return new Promise((resolve, reject) => {
		const clientRequest = http.request(options, (d) => {
			let response = {
				// Response object.
				statusCode: d.statusCode,
				headers: d.headers,
				data: []
			};
			d.on('data', (chunk) => {
				// Collect response body data from stream
				response.data.push(chunk);
			});
			d.on('end', () => {
				// Resolve on end by joining buffer
				if (response.data.length) {
					response.data = Buffer.concat(response.data).toString();
					try {
						response.data = JSON.parse(response.data);
						response.data.body = response.data.body ? JSON.parse(response.data.body) : {};
					} catch (error) {
						// Silently fail if response is not JSON.
					}
				}
				resolve(response);
			});
		}); //eo request

		clientRequest.on('error', (error) => {
			// Reject on request error.
			reject(_log([ error ]));
		});

		if (data) {
			// Write request data body if present.
			clientRequest.write(data);
		}

		clientRequest.end(); // Close HTTP connection, send off
	}); //eo promise
}

/**
 * Post a request to Penta for servicing
 */
module.exports = { createAction, dispatch, httpRequest };
