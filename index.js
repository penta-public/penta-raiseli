const http = require('http');
const testEndpoint = require('./tests/test-endpoint');
const Penta = require('./penta-raiseli');

const d = { email: 'smelnikoff@mac.com', json: true, x: 'string' };

http.createServer(testEndpoint).listen(8000);

Penta.httpRequest('http://localhost:8000', d).then((d) => console.log(JSON.stringify(d)));
