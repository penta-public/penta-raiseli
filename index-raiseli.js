"use strict";
/**
 * Main driver to spin up a Fastify server and then connect Cote to it via the /raiseli route
 * this route is a simple Echo function that takes all incoming posts and then sends back the
 * incoming json body
 *
 */
const fastify = require("fastify")({ logger: false });
//const Penta = require("../penta-raiseli");

//fastify configuration object
const testGroup = "RAISELI";
const port = 9000;
const paths = { http: "/http", raiseli: "/raiseli" };
const path = require("path");

const raiseliRoute = {
  path: "/raiseli", //exports the fastify-cote options, filepath relative to plugins/
  data: null,
  modules: {
    cote: "server/routes/raiseli",
    filepath: "server/routes/raiseli", //pseudo-filepath, actually points to fastify-cote module in plugins
    pluginPath: "server/plugins/fastify-cote" //pseudo-filepath, actually points to fastify-cote module in plugins
  },
  schema: null
};

const modulePaths = modules => {
  Object.entries(modules).forEach(([key, value]) => {
    modules[key] = path.join(__dirname, value);
  });
};

const init = async () => {
  // Run the server!
  let _path = null;
  try {
    console.log(
      `\n========================  ${testGroup}  =========================`
    );
    modulePaths(raiseliRoute.modules);
    fastify.register(require(raiseliRoute.modules.pluginPath), raiseliRoute);
    console.log(`>> registered ${raiseliRoute.modules.pluginPath}`);
    fastify.register(
      require(raiseliRoute.modules.filepath).route,
      raiseliRoute
    );
    console.log(`>> registered ${raiseliRoute.modules.filepath}`);
    await fastify.listen(port);
    console.log(
      `>> Raiseli server listening on ${fastify.server.address().port}`
    );
    console.log(`fastify routes\n ${fastify.printRoutes()}`);
  } catch (err) {
    console.log(err);
    process.exit(0);
  }
};

init();
