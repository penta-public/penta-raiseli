<h1 align="center">Welcome to penta-raiseli 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-3.3.0-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: UNLICENSED" src="https://img.shields.io/badge/License-UNLICENSED-yellow.svg" />
  </a>
</p>

> Interface code for Raiseli to send Posts to Penta platform

### 🏠 [Homepage](https://gitlab.com/penta-public/penta-raiseli#readme)

## Install

```sh
npm install
```

## Run tests

```sh
npm run test
```

## Author

👤 **Steve Melnikoff <steve.melnikoff@penta.global>**

* Website: async answers =>
    answers.authorGithubUsername !== projectInfos.githubUsername
      ? getAuthorWebsiteFromGithubAPI(answers.authorGithubUsername)
      : projectInfos.authorWebsite

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://gitlab.com/penta-public/penta-raiseli/issues).

## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_